﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using System.Threading;

namespace Horizon_Launcher
{
    class Program
    {
        static void Main(string[] args)
        {
            const string HELP =                                                                                                     //Help Message
                            "Horizon Launcher/r/n" +
                            "Version 1.0/r/n" +
                            "Author: Chris Anthony/r/n" +
                            "Date: 07/21/2017\r\n\r\n" +
                            "This console application launches the current version of Horizon.\r\n" +
                            "This version launches 2017_01 with GUI version 7.2929.1.79.\r\n" +
                            "This application accepts the following switches:\r\n" +
                            "/?, /help --help\r\n" +
                            "     These display the help document.\r\n\r\n" +
                            "/url\r\n" +
                            "    This allows you to pass a string url to go to a different url with out\r\n" +
                            "    without having to recompile the application.\r\n\r\n" +
                            "Congressional Bank, 2017";
            string systemDrive = Path.GetPathRoot(Environment.GetFolderPath(Environment.SpecialFolder.System));                     //System Drive Letter
            string horizonURL = "http://cbfs1.congressionalbank.local/legasuite/7.2929.1.79/Production/HorizonX.html";              //Current Horizon Path
            string userDir = System.Environment.GetEnvironmentVariable("USERPROFILE") + @"\Documents\";
            string internetExplorerDir = "";                                                                                        //Placeholder for IE directory
            
            //Error logging and reporting method

            void LogErrorMessage(string error)
            {
                using(StreamWriter sw = File.AppendText(userDir + "HorizonLauncherErrorLog.txt"))
                {
                    DateTime today = DateTime.Now;
                    error = "\r\n" + today.ToString() + "\r\n" + error;
                    sw.WriteLine(error);
                    string errorMessage = "The Horizon Launcher has encountered an error and has been logged.  Please contact the Service Desk for assistance.  The error is:\r\n\r\n" + error;
                    string errorHeading = "Horizon Lancher Error";
                    MessageBox.Show(null, errorMessage, errorHeading);
                }
            }

            try
            {
                //Check for switch
                for (int i = 0; i <= args.GetUpperBound(0); i++)
                {
                    switch (args[i])
                    {
                        case "/?":
                        case "/help":
                        case "--help":
                            Console.WriteLine(HELP);
                            Environment.Exit(0);
                            break;
                        case "/url":
                            horizonURL = args[i + 1];
                            break;
                        default:
                            break;
                    }
                }
            }
            catch(Exception ex)
            {
                LogErrorMessage("Error accessing parameter switch" + ex.ToString()); 
            }

            try
            {
                //Check for 64 bit
                if (Environment.Is64BitOperatingSystem)
                {
                    internetExplorerDir = systemDrive + @"Program Files (x86)\Internet Explorer\iexplore.exe";
                }
                else
                {
                    internetExplorerDir = systemDrive + @"Program Files\Internet Explorer\iexplore.exe";
                }
            }
            catch(Exception ex)
            {
                LogErrorMessage("Error checking for bit type of Windows" + ex.ToString());
            }

            try
            {
                using (Process hzExplorer = new Process())
                {
                    SHDocVw.InternetExplorer IE = new SHDocVw.InternetExplorer();
                    IE.Visible = true;
                    IE.Navigate(horizonURL);
                    Thread.Sleep(5000);
                    IE.Quit();
                }
            }
            catch(Exception ex)
            {
                LogErrorMessage("Error starting IE or Horizon" + ex.ToString());
            }
        }
    }
}